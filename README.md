# nodejs-httpserver

## Introduction

Bonjour, ceci est un exercice qui a pour but de tester vos connaissances sur Node.js, plus précisement sur la partie création de serveur http.

La création de serveur http avec Node.js étant plutôt fastitieuse, nous vous demanderons d'utiliser Express.js, si vous êtes plus à l'aide avec un autre framework comme Koa.js vous pouvez vous en servir.

Pour le rendu de template, vous pouvez utiliser Express Handlebars, mais là aussi si vous êtes plus à l'aise avec un autre moteur de template vous pouvez en choisir un autre.

## Prérequis
- [ ] Avoir des bases en Node.js et NPM
- [ ] Connaître les fondamentaux de JavaScript (Objets, Tableaux, Conditions, etc.)
- [ ] Le format JSON

## Ce que l'application doit contenir

Une vue HTML qui affiche une liste de membres (nom, email et statut).
Une Rest API.

Voici le tableau des membres à afficher, vous pouvez le placer dans votre code en dur, inutile ici d'utiliser une base de données ou un api déjà existante.

```
[
    {
        "id": 1,
        "name": "John",
        "email": "john@gmail.com",
        "status": "active"
    },
    {
        "id": 2,
        "name": "Jane",
        "email": "jane@gmail.com",
        "status": "inactive"
    },
    {
        "id": 3,
        "name": "Joe",
        "email": "joe@gmail.com",
        "status": "active"
    }
]
```

Vous devez créer une page d'accueil qui liste les membres, vous pouvez vous servir de cet exemple pour reproduire le rendu :
![Rendu frontend](https://media.odalys-vacances.com/plus/IMAGES/tests/demo-1-frontend-result.jpg "Rendu frontend")

Vous pouvez utiliser le moteur de template de votre choix, pour cet excercice nous vous recommandons Handlbars qui fonctionne très bien avec Express.js.

Vous devez également mettre à disposition des équipes frontend une API.
Voici les routes que vous devez créer :
/api/members doit renvoyer au format JSON la liste de tous les membres
![Membre trouvé](https://media.odalys-vacances.com/plus/IMAGES/tests/demo-1-api-members.jpg "Membre trouvé")

/api/members/:id doit renvoyer les informations du membre dont l'id est passé en paramètre de l'url.
![Membre trouvé](https://media.odalys-vacances.com/plus/IMAGES/tests/demo-1-api-members-id-200.jpg "Membre trouvé")

Si l'id ne correspond à aucun membre, il faudra renvoyer un message d'erreur pour l'indiquer.
![Pas de membre trouvé](https://media.odalys-vacances.com/plus/IMAGES/tests/demo-1-api-members-id-400.jpg "Pas de membre trouvé")

Vous devez indiquer dans votre README.md la commande à taper pour lancer le serveur.

## Liens utiles

- [ ] [Node.js](https://nodejs.org/en/)
- [ ] [La documentation de Express.js](https://expressjs.com/fr/)
- [ ] [Express.js sur NPM](https://www.npmjs.com/package/express)
- [ ] [Nodemon sur NPM, pour relancer automatiquement le serveur lorsque vous modifiez le code](https://www.npmjs.com/package/nodemon)
- [ ] [Bootstrap 5](https://getbootstrap.com/)
- [ ] [Express Handlebars](https://www.npmjs.com/package/express-handlebars)